public class Main {
    public static void main(String[] args) {
        Basket basket = new Basket();
        Basket basket1 = new Basket();

        basket1.add("картошка", 10);
        basket1.add("груша", 120, 2, 2);
        basket1.add("томат", 130, 4, 2);
        basket.add("молоко", 40);
        basket.add("апельсин", 100, 10, 5);
        basket.add("морковь", 20);
        basket.add("яблоко", 120, 3, 2);
        basket.add("йогурт", 100, 2, 3);
        basket.add("макороны", 40, 3, 3);

        System.out.println("общая сумма: " + Basket.getTotalPriceAllProducts() + " руб.");
        System.out.println("общее колличество продуктов в корзине " + Basket.getTotalCountProducts() + " шт");
        System.out.println("средняя стоимость товара составит: " + Basket.calcPriceAverage() + " руб.");
        System.out.println("средняя стоимость корзины составит: " + Basket.calcPriceAverageOneBasket() + " руб.");

    }
}
